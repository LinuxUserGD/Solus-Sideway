extends Label
const SCENE_PATH : String = "res://node_3d.tscn"
var pressed: bool = false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	get_node("Button").connect("pressed", Callable(self, "_on_button_pressed"))

func _process(delta: float) -> void:
	if not pressed:
		get_node("Button").emit_signal("pressed")
	set_process(false)

func _on_button_pressed():
	ResourceLoader.load_threaded_request(SCENE_PATH)
	var scene = ResourceLoader.load_threaded_get(SCENE_PATH)
	var instance = scene.instantiate()
	for i in get_tree().get_root().get_children():
		i.queue_free()
	get_tree().get_root().add_child(instance)
