extends WorldEnvironment

var speed1: float = 0.0
#var speed2: float = 0.0
const max_speed: float = 50
static var try: int = 1
static var checkpoint: int = 0
static var playback_pos: float = 0.0
var audioplayer: AudioStreamPlayer = null
const SCENE_PATH : String = "res://menu.tscn"
const CURRENT_SCENE_PATH : String = "res://node_3d.tscn"
@onready var camera = $"cambase/Camera3D"
@onready var car = $"car"

func _ready():
	audioplayer = get_node("AudioStreamPlayer")
	if not audioplayer.playing:
		audioplayer.play(playback_pos)
	get_node("world/CSGCombiner3D/CSGCylinder3D/AnimationPlayer").current_animation = "move"
	get_node("Label").text = "Reset: Esc\n(" + str(try) + ")"
	if checkpoint == 1:
		get_node("car").position = get_node("pos").position + Vector3(0,0.066,0)
		get_node("frontwheel").position = get_node("pos").position + Vector3(-0.5,0.2,0)
		get_node("backwheel").position = get_node("pos").position + Vector3(0.5,0.2,0)
	elif checkpoint == 2:
		get_node("car").position = get_node("pos2").position + Vector3(0,0.066,0)
		get_node("frontwheel").position = get_node("pos2").position + Vector3(-0.5,0.2,0)
		get_node("backwheel").position = get_node("pos2").position + Vector3(0.5,0.2,0)
		get_node("world/AnimationPlayer").current_animation = "CHECKPOINT"
		get_node("world/AnimationPlayer2").current_animation = "CHECKPOINT"
	#get_node("Cylinder/MeshInstance3D/AnimationPlayer").current_animation = "move"
	#get_node("Cylinder2/MeshInstance3D/AnimationPlayer").current_animation = "move"

func _physics_process(_delta):
	if not car.position.y < 0:
		camera.position.y = camera.position.y*0.95 + car.position.y*0.05
	camera.position.x = car.position.x
	camera.rotation.x = atan((car.position.y-camera.position.y)/7.982)-0.2932153
	get_node("engine").pitch_scale = 0.6 + abs(speed1/max_speed) * 0.15
	get_node("engine").volume_db = -5 + abs(speed1/max_speed) * 3
	if Input.is_action_pressed("ui_left"):
		car.angular_velocity.z = 1.5
	if Input.is_action_pressed("ui_right"):
		car.angular_velocity.z = -1.5
	if Input.is_action_pressed("ui_up"):
		if speed1 >= -max_speed:
			speed1 = -max_speed
		#if speed2 >= -max_speed:
		#	speed2 = -max_speed
		if get_node("frontwheel").angular_velocity.z > speed1:
			get_node("frontwheel").angular_velocity.z = speed1
		#if get_node("backwheel").angular_velocity.z > speed2:
		#	get_node("backwheel").angular_velocity.z = speed2
	elif Input.is_action_pressed("ui_down"):
		if speed1 <= max_speed:
			speed1 = max_speed
		#if speed2 <= max_speed:
		#	speed2 = max_speed
		if get_node("frontwheel").angular_velocity.z < speed1:
			get_node("frontwheel").angular_velocity.z = speed1
		#if get_node("backwheel").angular_velocity.z < speed2:
		#	get_node("backwheel").angular_velocity.z = speed2
	else:
		speed1 = get_node("frontwheel").angular_velocity.z
		#speed2 = get_node("backwheel").angular_velocity.z
	if Input.is_action_just_pressed("reset"):
		try += 1
		reload()


func _on_area_3d_body_entered(_body):
	get_node("world/AnimationPlayer").current_animation = "move"
	get_node("world/AnimationPlayer2").current_animation = "move"
	get_node("Area3D").queue_free()


func _on_checkpoint_body_entered(_body) -> void:
	if not checkpoint > 1:
		get_node("world/checkpoint/AnimationPlayer").current_animation = "color"
		get_node("checkpoint").play()
		checkpoint = 1
		get_node("world/checkpoint/checkpoint").queue_free()


func _on_checkpoint_2_body_entered(_body) -> void:
	if not checkpoint > 2:
		get_node("world/checkpoint2/AnimationPlayer").current_animation = "color"
		get_node("checkpoint").play()
		checkpoint = 2
		get_node("world/checkpoint2/checkpoint2").queue_free()

func _on_goal_body_entered(_body) -> void:
	quit_game()


func quit_game() -> void:
	checkpoint = 0
	try = 1
	playback_pos = 0.0
	ResourceLoader.load_threaded_request(SCENE_PATH)
	var scene = ResourceLoader.load_threaded_get(SCENE_PATH)
	var instance = scene.instantiate()
	for i in get_tree().get_root().get_children():
		i.queue_free()
	get_tree().get_root().add_child(instance)

func reload() -> void:
	playback_pos = audioplayer.get_playback_position()
	ResourceLoader.load_threaded_request(CURRENT_SCENE_PATH)
	var scene = ResourceLoader.load_threaded_get(CURRENT_SCENE_PATH)
	var instance = scene.instantiate()
	for i in get_tree().get_root().get_children():
		i.queue_free()
	get_tree().get_root().add_child(instance)
